
var deferredPrompt;
var enableNotificationsButtons = document.querySelectorAll('.enable-notifications');

if (!window.Promise) {
  window.Promise = Promise;
}

if ('serviceWorker' in navigator) {
  navigator.serviceWorker
    .register('/sw.js')
    .then(function () {
      console.log('Service worker registered!');
    })
    .catch(function(err) {
      console.log(err);
    });
}

window.addEventListener('beforeinstallprompt', function(event) {
  console.log('beforeinstallprompt fired');
  event.preventDefault();
  deferredPrompt = event;
  return false;
});


function displayConfirmNotification() {
  if('serviceWorker' in navigator) {
    var options = {
      body: 'You successfully subscribed to our Notification service!',
      image: 'https://yahadzohim.com/efi.95082d569702942fa21b.jpg',
      icon: '/src/images/icons/app-icon-96x96.png',
      dir: 'ltr',
      lang: 'en-US',
      vibrate: [100, 50, 200],
      badge: '/src/images/icons/app-icon-96x96.png', /// el icon que indica que an llegado notifictiones (en chiquito arriva)
      tag: 'efi-confirm-notification',  /// tagear la notificatiopara no hacer spam en el pelephone
      renotify: false,  // esto es si queremos que no bibre la notification cuando ya hay una notificacion que 
      // todabia no se ha abierto en el telefono (esto tiene que tener 'tag' parameter)
      actions: [
        {
          action: 'confirm', title: 'Okay', icon: '/src/images/icons/app-icon-96x96.png',
        },
        {
          action: 'confirm', title: 'Cancel', icon: '/src/images/icons/app-icon-96x96.png',
        }
      ]
    };  
    navigator.serviceWorker.ready.then(function (sw_registrations) {
      sw_registrations.showNotification('Succesfully subscribed', options)
    });
  }
}

function confirmPushSub() {
  if(!('serviceWorker' in navigator)) {
    alert('Oh no! your browser not support notifications!! Download Chrome for free!')
    return;
  }
  var reg;
  navigator.serviceWorker.ready.then(function (sw_regist) {
    reg = sw_regist;
    return sw_regist.pushManager.getSubscription();
  }).then(function (sub) {
    if(sub === null) {
      var vapidPublicKey = 'BJecTGZ8M79J9rZKxjVDqbXqFAl9hW8EcSDlHS0aFCvR5SspZ3sDelBeGaknU6rv7DxaYdvqkUuSQsPwYGrkOjU';
      var convertVapidPublicKey = urlBase64ToUint8Array(vapidPublicKey);
      return reg.pushManager.subscribe({
        userVisibleOnly: true,
        applicationServerKey: convertVapidPublicKey,
      });
    } else {
      // 
    }
  }).then(function (sub) {
    fetch('https://task-list-f3c4a.firebaseio.com/subscriptions.json', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      },
      body: JSON.stringify(sub)
    })
    .then(function(res) {
      console.log('res before displayConfirmNotification');
      console.log(res);
      if(res.ok) {
        displayConfirmNotification();
      }
    })
    .catch(function (err) {
      console.log(err);
    });
  })
  ;
  
}

function askForNotificationsPermition() {
  Notification.requestPermission(function (result) {
    console.log('User Choice', result);
    if(result !== 'granted') {
      console.log('No notification permission granted!');
    } else {
      confirmPushSub();
      // displayConfirmNotification();
    }
  });
}

if ('Notification' in window && 'serviceWorker' in navigator) {
  for (let index = 0; index < enableNotificationsButtons.length; index++) {
    var btn = enableNotificationsButtons[index];
    btn.style.display = 'inline-block';
    btn.addEventListener('click', askForNotificationsPermition);    
  }
}



