var functions = require('firebase-functions');
var admin = require('firebase-admin');
var cors = require('cors')({
    origin: true
});
var webpush = require('web-push');
var formidable = require('formidable');
var fs = require('fs');
var UUID = require('uuid-v4');
// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
var serviceAccount = require("./serviceAccountKey.json");

var gcconfig = {
    projectId: 'task-list-f3c4a',
    keyFilename: 'serviceAccountKey.json'
}

var gcs = require('@google-cloud/storage')(gcconfig);

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://task-list-f3c4a.firebaseio.com"
});

exports.storePostData = functions.https.onRequest(function (request, response) {
    cors(request, response, function () {
        var uuid = UUID();
        var formData = new formidable.IncomingForm();
        formData.parse(request, function (err, fields, files) {
            fs.rename(FileList.file.path, '/temp/' + files.file.name);
            var bucket = gcs.bucket('gs://task-list-f3c4a.appspot.com');
            bucket.upload('/tmp/' + files.file.name, {
                uploadType: 'media',
                matadata: {
                    matadata: {
                        contentType: file.file.type,
                        firebaseStorageDownloadTokens: uuid
                    }
                },
                function (err, file) {
                    if (!err) {
                        admin.database.ref('posts').push({
                                id: fields.id,
                                title: fields.title,
                                location: fields.location,
                                image: 'https://firebasestorage.googleapis.com/v0/b/' + bucket.name + encodeURIComponent(file.name) + '?alt=media&token=' + uuid,
                            })
                            .then(function () {
                                webpush.setVapidDetails('mailto:secorrenti@gmail.com',
                                    'BJecTGZ8M79J9rZKxjVDqbXqFAl9hW8EcSDlHS0aFCvR5SspZ3sDelBeGaknU6rv7DxaYdvqkUuSQsPwYGrkOjU',
                                    'oDXyKyObjUubSmYxeZ2b_Hg7pm91pLAyFU0UY42qMAY');
                                return admin.database().ref('subscriptions').once('value');
                            })
                            .then(function (subscriptions) {
                                subscriptions.forEach(function (sub) {
                                    var pushConfig = {
                                        endpoint: sub.val().endpoint,
                                        keys: {
                                            auth: sub.val().keys.auth,
                                            p256dh: sub.val().keys.p256dh
                                        }
                                    };
                                    webpush.sendNotification(pushConfig, JSON.stringify({
                                            title: 'New Post',
                                            content: 'New Post added!',
                                            openUrl: '/help'
                                        }))
                                        .catch(function (err) {
                                            console.log(err);
                                        });
                                });
                                response.status(201).json({
                                    message: 'Data stored',
                                    id: fields.id
                                });
                            })
                            .catch(function (err) {
                                response.status(500).json({
                                    error: err
                                });
                            });
                    } else {

                    }
                }
            });
        });
    });
});
